---
layout: post
title: "Dr. Shannon Chavez"
date: 2017-01-05
categories:
  - Client
description:
image: /assets/img/drshannon-mockup.jpg
image-sm: /assets/img/drshannon-mockup.jpg
---
Maintain, monitor and manage Dr. Chavez’s online brand through conversion optimization and Internet marketing & reputation management. We helped increased the site conversions by over 35% and grew her email list to over 26,000 contacts.

<div class="landing_preview"><a href="http://drshannonchavez.com/" target="_blank" class="preview__more">View Site</a></div>
