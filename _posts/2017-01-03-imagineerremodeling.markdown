---
layout: post_ad
title: "Imagineer Remodeling"
date: 2017-01-03
categories:
  - Client
description:
image:  /assets/img/imagineerremodeling-mockup.jpg
image-sm:  /assets/img/imagineerremodeling-mockup.jpg
---
Coordinated branding, content creation, site development, conversion marketing and on-going site management for a large European-style custom home construction and remodeling company in Los Angeles, CA. We were able to help increase website leads by 25% with paid conversion marketing, blogging and email marketing.

<div class="landing_preview"><a href="http://imagineerremodeling.com" target="_blank" class="preview__more">View Site</a></div>
<br>
## Branding and Digital Identity

As a brand new business, Imagineer Remodeling needed to create both a branding and digital identity. They wanted their brand to portray elegance and class while still being affordable and attainable to the average homeowner. After sorting out their new logo, we moved on to tackling the design of the website. We integrated a Wordpress theme, with a customized dashboard that allows the Imagineer team to easily update their site with new project galleries, articles and information. While website viewers can easily find projects they were looking for, information about home remodeling and tons of DIY tips for homeowners. We set up categories, tags and short codes within the content management system to make updating the site fast and searching for projects even easier!

## Content

One of their challenges was competing with more established construction companies with large advertising and marketing budgets that were using traditional advertising methods (TV, radio, direct mail, specialty magazines). Imagineer Remodeling decided to focus on both their website and online presence in addition to traditional advertising methods. This meant creating content – a lot of content – and regularly creating new, interesting, engaging content through a strategic content marketing campaign. Informational pages about the company, galleries and interactive design pages were styled to show off their work and give as much relevant detail as possible.

## Galleries & Search

We setup a light box so that viewers could browse the gorgeous project photos distraction-free in one of the many project galleries on the site.  We also integrated search filters to make it easier to find projects based on categories and tags.
